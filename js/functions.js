function isEmpty(data) {
	return data==undefined || data=='' || data==null
}
function toCamelCase(data){
  if(isEmpty(data))
    return ""
  return data.replace(/\s(.)/g, function($1) { return $1.toUpperCase() })
             .replace(/\s/g, '')
             .replace(/^(.)/, function($1) { return $1.toLowerCase(); })
}