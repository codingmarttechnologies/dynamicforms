pricingApp.controller('pricingController', [
  '$scope','localStorageService','$document',
  function ($scope,localStorageService,$document) {
    $scope.elements =[];
    $scope.form = {}
    $scope.isEmpty = function(data) {
      return data==undefined || data=='' || data==null
    }
    $scope.add_element={};
    $scope.sub_form={}
    $scope.element_types = ["text","textarea","select","radio","checkbox","date"]
    $scope.need_options = ["select","radio","checkbox"]
    $scope.layouts = ["Single","Multiple"]
    $scope.validation_rules = ["number","required","mobile","email","selectRequired","string"]
    $scope.visible = ["Show","Hide","Show On","Hide On"]

    $scope.checkVisible = function(){
      $scope.need_options.indexOf($scope.add_element.type)>=0
    }

    $scope.setShowHideCondition = function(html,element){
      // console.log(html)
      html = $(html)
      if(element.type == 'radio'){
        ele = html.find('input[type="radio"]')
        ele = $scope.applyCondition(element.visible,ele,element,html)
        html.find('input[type="radio"]').replaceWith(ele)
      } else if (element.type == 'textarea') {
        ele = html.find('textarea')
        ele = $scope.applyCondition(element.visible,ele,element,html)
        html.find('textarea').replaceWith(ele)
      } else if (element.type == 'select') {
        ele = html.find('select')
        ele = $scope.applyCondition(element.visible,ele,element,html)
        html.find('select').replaceWith(ele)
      } else {
        ele = html.find('input')
        ele = $scope.applyCondition(element.visible,ele,element,html)
        html.find('input').replaceWith(ele)
        // html.append("{{form."+element.label+"}}")
        // html.append("{{form}}")
      }
      console.log(html.prop('outerHTML'))
      return angular.element("<div>").append(html).html()
      // return html.prop('outerHTML')
    }

    $scope.applyCondition = function (condition,element,elementObject,html){
      if(condition == "Show On"){
        conditionString = "form."+elementObject.conditionShowLeft.label.toLowerCase()+"=='"+elementObject.conditionShowRight+"'"
        conditionString = $('<div/>').html(conditionString).text();
        console.log(conditionString)
        html.attr("ng-show",conditionString)
      }

      if(condition == "Hide On"){
        conditionString = elementObject.conditionHideLeft.label.toLowerCase()+"=="+elementObject.conditionHideRight
        conditionString = $('<div/>').html(conditionString).text();
        console.log(conditionString)
        html.attr("ng-hide",conditionString)
      }
      return ele;
    }

    $scope.addElement=function(){
      html = ""
      if($scope.element_types.indexOf($scope.add_element.type)>=0){
        html = $scope.add_element_options($scope.add_element)
        html = $scope.setShowHideCondition(html,$scope.add_element)
      }
      console.log(html)
      $scope.add_element.html = html
      $scope.elements.push(angular.copy($scope.add_element));
      var tempElement = $scope.elements
      $scope.elements = {}
      $scope.elements = tempElement
      $scope.add_element = {}
    }

    $scope.mark_as_subform = function(elements,sub_form) {

      subform_name = $scope.sub_form.name
      if($scope.sub_form.make==true){
        $scope.element_types.push(subform_name)
      }
      $scope.sub_form.elements = $scope.elements
      localStorageService.set(subform_name,$scope.sub_form)
      $scope.elements=[]
      $scope.add_element={}
    }

    $scope.check_multiple = function(elementObject,element){
        append_ele = element.children(":first")
       if(elementObject.layout=="Multiple"){
          element.append("<a ng-click='add_multiple($event,element)' class='btn btn-small btn-info'>Add</a>")
          append_ele.attr("multiple-times","true")
          append_ele.attr("max-count",(elementObject.max_size=="")? "0":elementObject.max_size)
        }
        else if(append_ele.attr("multiple-times")=="true"){
          
        }
      }

    $scope.add_multiple = function($event,elementObj){
      refObj = angular.copy(elementObj)
      old_html = angular.element(refObj.html)
      old_html_container = angular.element("<div>").html(old_html)  
      delete refObj.layout
      html = $scope.add_element_options(refObj)
      old_html_container.children(":first").after(html)
      elementObj.html = old_html_container.html()
    }

    $scope.generate_dynamic_attr = function(element,elementObj){
      inputElement  = element.find("input,select,textarea")
      model_name = "form."+toCamelCase(elementObj.label)
      inputElement.attr("validator",elementObj.validation_string)
      inputElement.attr("ng-model",model_name)
      inputElement.addClass(elementObj.class_name)
      inputElement.attr('placeholder',elementObj.placeholder)
    }
      $scope.generate_select=function(elementObject){
          element=angular.element("#select_template").clone()
          element.find("select").addClass(elementObject.class_name);
          element.find("label").text(elementObject.label)
          //custom_options = JSON.parse(elementObject.options)
          options_name = elementObject.label+"_options"
          $scope[options_name] = JSON.parse(elementObject.option_value)
          model_name = "form."+toCamelCase(elementObject.label)
          element.find("select").attr('ng-options','option.value as option.name for option in '+options_name);
          $scope.generate_dynamic_attr(element,elementObject)
          return element.html()
      }
      $scope.generate_textarea=function(elementObject){
        element=angular.element("#textarea_template").clone()
        is_multiple = $scope.check_multiple(elementObject,element)
        element.find("label").text(elementObject.label)
        model_name = "form."+toCamelCase(elementObject.label)
        element.find('textarea').attr("ng-model", model_name)
        element.find("textarea").addClass(elementObject.class_name);
        $scope.generate_dynamic_attr(element,elementObject)
        return element.html()
      }
      $scope.generate_text=function(elementObject){
          element=angular.element("#text_template").clone()
          $scope.check_multiple(elementObject,element)
          input_block = element.children(":first")
          $scope.generate_dynamic_attr(element,elementObject)
          element.find("label").text(elementObject.label)
          // if(input_block.attr('multiple-times')=="true")
          //   element.children(":first").append(angular.element("<a class='delete_multiple'>delete</a>"))
          model_name = "form."+toCamelCase(elementObject.label)
          element.find('textarea').attr("ng-model", model_name)
          return element.html()
      }
      $scope.generate_radio=function(elementObject){
        elements = ""
        print = ""
          angular.forEach(JSON.parse(elementObject.option_value),function(value,key){
            element=angular.element("#radio_template").clone()
            element.find("label").append(key)   
            element.find("input").attr("ng-model",toCamelCase(elementObject.label))
            element.find('input').attr("value",value)
            model_name = "form."+toCamelCase(elementObject.label)
            element.find('input').attr("ng-model", model_name)
            element.find('input').attr("name",toCamelCase(elementObject.label))
            element.find('input').addClass(elementObject.class_name)
            elements += element.html()
          })
          return elements
      }
      $scope.generate_checkbox=function(elementObject){
        element=angular.element("#checkbox_template").clone()
        element.find("label").append(elementObject.label)   
        model_name = "form."+toCamelCase(elementObject.label)
        element.find('input').attr("ng-model", model_name)    
        element.find('input').addClass(elementObject.class_name)
        return element.html()
      }

      $scope.generate_sub_form = function(elements,elementObject){
        element = angular.element("#sub_form_template").clone()
        $scope.check_multiple(elementObject,element)
        element.find(".title").html(elementObject.label)
        innerContent = element.find(".innerContent")
        if(elements.length>0){
          angular.forEach(return_element,function(rele,index){
            innerContent.append(rele)
          })
        }
        return element.html()
      }

      $scope.deleteElement = function(index) {
        $scope.elements.splice(index,1)
      }

    $scope.add_element_options = function(elementObject){
      localStorageData = false
      elementType = elementObject.type
      switch(elementType){
        case "select":
          return $scope.generate_select(elementObject)
        case "textarea":
          return $scope.generate_textarea(elementObject)
        case "text":
          return $scope.generate_text(elementObject)
        case "radio":
          return $scope.generate_radio(elementObject)
        case "checkbox":
          return $scope.generate_checkbox(elementObject)
        default:
          if(localStorageService.get(elementType)!=undefined){
            elements = localStorageService.get(elementType).elements
            return_element = []
            angular.forEach(elements,function(ele,index){
              return_element.push($scope.add_element_options(ele))
            })
            return $scope.generate_sub_form(return_element,elementObject)
          }else{
            alert("Wrong Data type");
            return false;
          }
      }

      
      // if(["select","textarea"].indexOf(elementType)>=0){
      //   elementHTML.find(elementType).addClass(elementObject.class_name)
      //   if (elementType=='select' && !isEmpty(elementObject.options)) {
      //     elementHTML.find(elementType).attr('ng-options','option.value as option.name for option in element.options');
      //   }
      // } else {
      //   if (elementType == 'text') {
      //     elementHTML.find("label").text(elementObject.label)
      //   } else {
      //     elementHTML.find("label").append(elementObject.label)
      //   }
      //   elementHTML.find('input').attr('placeholder',elementObject.placeholder)
      //   elementHTML.find('input').addClass(elementObject.class_name)
      // }
      // if (!isEmpty(elementObject.custom_attributes)) {
      //     elementHTML.find(elementObject.type).attr(JSON.parse(elementObject.custom_attributes))
      // }
      return localStorageData ? elementHTML : elementHTML.html()
    }

  }
]);