(function() {
	angular.module('validation.rule', ['validation'])
	.config(['$validationProvider',
		function($validationProvider) {
			var expression = {
				required: function(value) {
					return !!value;
				},
				checkBoxRequired: function(value, scope, element, attrs) {
					return ($(element).prop('checked') == true)? true:false;
				},
				minValue: function(value, scope, element, attrs) {
					return (parseFloat(element[0].min) <= parseFloat(value)) ? true:false;
				},
				maxValue: function(value, scope, element, attrs) {
					return (parseFloat(element[0].max) >= parseFloat(value)) ? true:false;
				},
				minDate: function(value, scope, element, attrs) {
					var todayDate = new Date();
					var selectedDate = new Date(value);
					return  selectedDate > todayDate.setDate(todayDate.getDate() + parseInt(attrs.mindays))
				},
				selectRequired: function(value, scope, element, attrs){
					return (value != "? undefined:undefined ?")? true:false
				},
				fileRequired : function(value,scope,element,attrs){
					return ($(element).val()!="")
				},
				phoneNumber: function(value, scope, element, attrs){
					return (value != undefined)? true:false
				},
				equals:function(value,scope,element,attrs){
					equals = $("input[name='"+attrs.equals+"']").val()
					return (equals==element.val())
				},
				maxlength:function(value,scope,element,attrs){
					//return true;
					return value.length <= Number(attrs.maxlength)
				},
				activityDurationValidation:function(value,scope,element,attrs){
					days = parseInt(jQuery("select[name='"+attrs.name+"_days']").val())==0
					hours = parseInt(jQuery("select[name='"+attrs.name+"_hours']").val())==0
					mins = parseInt(jQuery("select[name='"+attrs.name+"_mins']").val())==0
					return !(days && hours && mins)
				},
				checkBox: function(value,scope,element,attrs){
					return_val=true
					min = parseInt(attrs.minRequired)
					max = parseInt(attrs.maxRequired)
					if(isNaN(min)){
						min=1
					}
					return_val = $("input[name='check"+attrs.name+"']:checked").length>=min
					if(!isNaN(max)){
						return_val = (return_val && $("input[name='check"+attrs.name+"']:checked").length<=max)
					}
					return return_val
				},
				radio: function(value,scope,element,attrs){
					return($("input[name='"+attrs.name+"']:checked").length>0)
				},
				validate_dynamic_radio: function(value,scope,element,attrs){
					element_name = attrs.validName
					return $(element).find('input[name="'+element_name+'"]:checked').length>0
				},
				multiSelect: function(value,scope,element,attrs){
					return_val=true
					min = parseInt(attrs.minRequired)
					max = parseInt(attrs.maxRequired)
					if(isNaN(min)){
						min=1
					}
					selected = $("select[name='select"+attrs.name+"']").val();
					return_val = (selected!=undefined) && (selected.length>=min)
					if(!isNaN(max)){
						return_val = return_val && (selected!=undefined) && (selected.length<=max)
					}
					return return_val
				},
				percentage:function(value,scope,element,attrs){
					return_val=true
					if ((parseInt(value) >= 0 )&& (parseInt(value) <= 100)) {
						return_val=true
					} else {
						return_val=false
					}
					return return_val;
				},
				validate_dynamic:function(value,scope,element,attrs){
					return_val=true
					min = parseInt(attrs.minRequired)
					max = parseInt(attrs.maxRequired)
					if(isNaN(min)){
						min=1
					}
					dynamicScope = attrs.dynamicScope.split("-")
					valueCount =0
					switch(dynamicScope.length){
						case 0:
						return false
						case 1:
						valueCount = scope.$parent[dynamicScope[0]].length
						break;
						case 2:
						if (scope.$parent[dynamicScope[0]]==undefined)
							return false;
						if(scope.$parent[dynamicScope[0]][dynamicScope[1]]==undefined)
							return false
						valueCount = scope.$parent[dynamicScope[0]][dynamicScope[1]].length
						break;
						default:
						return false;

					}
					return_val = (valueCount>=min)
					if(!isNaN(max)){
						return_val = return_val && valueCount<=max
					}
					return return_val
				},
				ignore: function(value,scope,element,attrs){
					return true;
				},
				pan:function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/^([a-zA-Z]){5}([0-9]){4}([a-zA-Z]){1}?$/)!=null
				},
				ifsc:function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/[A-Z|a-z]{4}[0][\d]{6}$/)!=null
				},
				url: function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/[-a-zA-Z0-9@:%_\+.~#?&//=]{2,256}\.[a-z]{2,6}\b(\/[-a-zA-Z0-9@:%_\+.~#?&//=]*)?/gi)!=null
				},
				email: function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/)!=null
				},
				number: function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/^\d+$/)
				},
				string: function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/^([a-zA-Z ]*)$/)
				},
				mobile:function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/^[0-9]{10}$/)
				},
				mobile_IN:function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/^(?:(?:\+|0{0,2})91(\s*[\-]\s*)?|[0]?)?[789]\d{9}$/)
				},
				mobile_AE:function(value, scope, element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/^[0-9]{9}$/)
				},
				phone:function(value,scope,element){
					element.val(element.val().trim())
					value = element.val()
					return value.trim()=="" || value.match(/^[0-9]{6,11}$/)
				},
				rangeRequired:function(value,scope,element,attrs){
					var validate_for =  attrs.validateFor 
					from_val = jQuery("[name='"+validate_for+"_from']").val()
					to_val = jQuery("[name='"+validate_for+"_to']").val()
					from_type = jQuery("[name='"+validate_for+"_from_type']").val()
					to_type = jQuery("[name='"+validate_for+"_to_type']").val()
					if((from_val=="" && to_val!="") ||(from_val!="" && to_val=="")){
						return false
					} else if ((from_val!=="" && to_val!=="")){
						if (from_val == to_val && from_type == to_type) {
							return false
						};
					}
					return true
				},
				selectAnyOneRole:function(value,scope,element,attrs){
					current_role = scope['$parent']['assign']['role']
					total = scope['$parent']['mappings'][current_role]
					if(total!=undefined){
						if(Object.keys(total).length==0)
							return true;
					}
					else{
						return true
					}
					data = scope['$parent']['assign']['manager']
					if(typeof(data)!="object")
						return false
					val_count = 0
					jQuery.each(data,function(key,val){
						if(val!=null)
							val_count++
					})
					if(val_count>1)
						return false;
					return true
				},
				inventory_pricing_dynamic:function(value,scope,element,attrs) {
					pricing_array = scope.$parent.pricings
					valid = true
					changeTab = false
					errors = []
					jQuery(".validation-invalid").remove()
					for(var cur_tab=0;cur_tab<pricing_array.length;cur_tab++){
						if(pricing_array[cur_tab].validity!==undefined)
							result = compare_dates(pricing_array[cur_tab].validity.from,pricing_array[cur_tab].validity.to)
						else
							result = false
						if(result==false){
							valid=false;
							errors.push({"element":"pricing_validity","message":"The start date cannot be later than the end date"})
						}
						/* Inventory Blackout Days Validation */
						blackoutdays_array =  pricing_array[cur_tab].blackOutDays
						if(!is_empty(blackoutdays_array)) {
							for(var rowCount=0;rowCount<blackoutdays_array.length;rowCount++){
								
								isValidRange = blackOutDaysValidation(pricing_array[cur_tab].validity.from,pricing_array[cur_tab].validity.to,
																		blackoutdays_array[rowCount].from, blackoutdays_array[rowCount].to)
								isValidDates = compare_dates(blackoutdays_array[rowCount].from, blackoutdays_array[rowCount].to)
								if(is_empty(blackoutdays_array[rowCount].from) || is_empty(blackoutdays_array[rowCount].to)){
									divId = 'removeBlackOut_'+rowCount;
									errors.push({"element":divId, "message": "Dates cannot be blank"})
								}
								else if(!isValidDates){
									divId = 'removeBlackOut_'+rowCount;
									errors.push({"element":divId, "message": "The start date cannot be later than the end date"})
								}
								else if(!isValidRange){
									blackOutDaysValid = false;
									divId = 'removeBlackOut_'+rowCount;
									errors.push({"element": divId, "message": "Blackout Days are not within valid ranges"})
								}
							}
						}
						blackoutdays_array = []
						if(!is_empty(pricing_array[cur_tab].validity.to)){
							date = moment(pricing_array[cur_tab].validity.to, "DD-MM-YYYY").toDate()
							toDayDate = moment(moment().format("DD-MM-YYYY"),"DD-MM-YYYY").toDate()
							if (date < toDayDate) {
								errors.push({"element":"pricing_validity","message":"Invalid date"})
								valid=false
							}
						}
						if(pricing_array[cur_tab].slots.length==0) {
								errors.push({"element":"activity_slots","message":"Please enter atleast one slot"})
								valid=false
						} else {
							for(var cur_slot=0;cur_slot<pricing_array[cur_tab].slots.length;cur_slot++){
								if (is_empty(pricing_array[cur_tab].slots[cur_slot].startingTime)) {
									valid=false;
									errors.push({"element":"slots_inventory_validation","message":"Starting Time is Invalid"})
									break;
								}
							}
						}
						if (valid==false && changeTab==false) {
							scope.$parent.tabIndex = cur_tab
							changeTab=true
						}
						if (valid==false) {
							break;
						}
					}
					if(errors.length>0){
						angular.forEach(errors,function(error){
							jQuery('[name="'+error.element+'"]').after("<p class='validation-invalid inventory-pricing-validation'>"+error.message+"</p>")
						})
					}
					return (valid && errors.length==0)
				},
				pricing_dynamic:function(value,scope,element,attrs) {

					saleType = scope.$parent.activity.pricingType
					localStorageErrorMsg = ""
					localStorageErrorTab = ""
					pricing_array = scope.$parent.pricing[saleType]
					//blackoutdays_array = pricing_array[0].blackOutDays
					activity_is_published = scope.$parent.activity.is_published
					valid = true
					blackOutDaysValid = true 
					errors = []
					blackouterrors = []
					jQuery(".pricing_messages").remove()
					for(var cur_tab=0;cur_tab<pricing_array.length;cur_tab++){
						subPricingType = pricing_array[cur_tab].subPricingType
						if(pricing_array[cur_tab].validity!==undefined)
							result = compare_dates(pricing_array[cur_tab].validity.from,pricing_array[cur_tab].validity.to)
						else
							result = false
						if(result==false){
							valid=false;
							errors.push({"element":"pricing_validity","message":"The start date cannot be later than the end date"})
						}
						if(!is_empty(pricing_array[cur_tab].validity)){
							date = moment(pricing_array[cur_tab].validity.to, "DD-MM-YYYY").toDate()
							toDayDate = moment(moment().format("DD-MM-YYYY"),"DD-MM-YYYY").toDate()
							if ((date < toDayDate) && (result!==false)) {
								errors.push({"element":"pricing_validity","message":"Invalid date"})
								valid=false
							}
						}
						blackoutdays_array = pricing_array[cur_tab].blackOutDays
						if(!is_empty(blackoutdays_array)) {
							for(var rowCount=0;rowCount<blackoutdays_array.length;rowCount++){
								isValidDates = compare_dates(blackoutdays_array[rowCount].from, blackoutdays_array[rowCount].to)
								if(!isValidDates){
									errors.push({"element":"blackoutdays_validity", "message": "The start date cannot be later than the end date"})
								}
								isValidRange = blackOutDaysValidation(pricing_array[cur_tab].validity.from,pricing_array[cur_tab].validity.to,
																		blackoutdays_array[rowCount].from, blackoutdays_array[rowCount].to)
								if(!isValidRange){
									blackOutDaysValid = false;
									divId = 'removeBlackOut_'+rowCount;
									errors.push({"element": divId, "message": "Blackout Days are not within valid ranges"})
								}
							}
							/*if(blackOutDaysValid==false) {
								errors.push({"element": "blackoutdays_validity", "message": "Blackout Days are not within valid ranges"})	
							}*/
						}
						if(saleType=="freeSale"){
							if (activity_is_published!=true) {
								if(is_empty(pricing_array[cur_tab].inventoryPerDay) || isNaN(pricing_array[cur_tab].inventoryPerDay)){
									valid=false;
									errors.push({"element":"maximum_capacity","message":"Please enter exact Inventory / Units"})
								} else if((pricing_array[cur_tab].inventoryPerDay > 9999 || pricing_array[cur_tab].inventoryPerDay < 1) && activity_is_published!=true){
									valid=false;
									errors.push({"element":"maximum_capacity","message":"Should be 1 to 9999"})
								}
							};
						} else {
							if(pricing_array[cur_tab].slots.length==0) {
								errors.push({"element":"activity_slots","message":"Please enter atleast one slot"})
								valid=false
							} else {
								slotsArray=[]
								for(var cur_slot=0;cur_slot<pricing_array[cur_tab].slots.length;cur_slot++){
									if(!is_empty(pricing_array[cur_tab].slots[cur_slot].startingTime)){
										if (slotsArray.indexOf(pricing_array[cur_tab].slots[cur_slot].startingTime)>=0) {
											valid=false;
											errors.push({"element":"slots_inventory_validation","message":"Fix the Duplicate time slot"})
											break;
										} else {
											slotsArray.push(pricing_array[cur_tab].slots[cur_slot].startingTime)
										}
									}
									if(activity_is_published==true){
										if (is_empty(pricing_array[cur_tab].slots[cur_slot].startingTime)) {
											valid=false;
											errors.push({"element":"slots_inventory_validation","message":"Starting Time is Invalid"})
											break;
										}
									} else {
										if (is_empty(pricing_array[cur_tab].slots[cur_slot].startingTime) || is_empty(pricing_array[cur_tab].slots[cur_slot].inventory)) {
											valid=false;
											errors.push({"element":"slots_inventory_validation","message":"Starting Time / Inventory is Invalid"})
											break;
										}
									}
								}
							}
						}
						kids_only = !(is_empty(pricing_array[cur_tab].kidsOnly) || pricing_array[cur_tab].kidsOnly == false)

						if(pricing_array[cur_tab].pricingData==undefined){
							errors.push({"element":"adult_market_price","message":"Enter Market Price"})
							errors.push({"element":"adult_commission","message":"Enter Commission"})
							if(kids_only){
								errors.push({"element":"child_market_price","message":"Enter Market Price"})
								errors.push({"element":"child_commission","message":"Enter Commission"})
							}
							if (subPricingType=='UnitBased') {
								errors.push({"element":"unit_market_price","message":"Enter Market Price"})
								errors.push({"element":"unit_commission","message":"Enter Commission"})
							};
							valid=false;
						}
						if(subPricingType=='PersonBased') {
							if(is_empty(pricing_array[cur_tab].minimumPersonRequired)){
								valid=false;
								errors.push({"element":"minimumPersonRequired","message":"Minimum persons required"})
							}
							if(!is_empty(pricing_array[cur_tab].pricingData)){

								// validate adult market pricing, for kids ignore validation
								if(is_empty(pricing_array[cur_tab].pricingData.adult)){
									if(!kids_only){
										valid = false;
										errors.push({"element":"adult_market_price","message":"Enter Market Price"})
										errors.push({"element":"adult_commission","message":"Enter Commission"})
									}
								}
								else{
									check_valid = validate_market_pricing(pricing_array[cur_tab].pricingData.adult.marketPrice,"adult_market_price",kids_only)
									if(!check_valid.result){
										errors.push(check_valid.errors)
										valid=false
									}
									check_valid = validate_commission(pricing_array[cur_tab].pricingData.adult.commission,"adult_commission",kids_only)
									if(!check_valid.result){
										errors.push(check_valid.errors)
										valid=false
									}
								}
								// validate child market price, for kids must validate
								if (is_empty(pricing_array[cur_tab].pricingData.child)) {
									if(kids_only){
										valid = false;
										errors.push({"element":"child_market_price","message":"Enter Market Price"})
										errors.push({"element":"child_commission","message":"Enter Commission"})
									}
								}
								else{
									check_valid = validate_market_pricing(pricing_array[cur_tab].pricingData.child.marketPrice,"child_market_price",!kids_only)
									if(!check_valid.result){
										errors.push(check_valid.errors)
										valid=false
									}
									check_valid = validate_commission(pricing_array[cur_tab].pricingData.child.commission,"child_commission",!kids_only)
									if(!check_valid.result){
										errors.push(check_valid.errors)
										valid=false
									}
									//validate if child market price is entered, child commission should also be entered
									if(is_empty(pricing_array[cur_tab].pricingData.child.marketPrice) && !is_empty(pricing_array[cur_tab].pricingData.child.commission) && !isNaN(pricing_array[cur_tab].pricingData.child.commission)){
										errors.push({"element":"child_market_price","message":"Enter Market Price"})
										valid = false
									}

									if(!is_empty(pricing_array[cur_tab].pricingData.child.marketPrice) && !isNaN(pricing_array[cur_tab].pricingData.child.marketPrice) && is_empty(pricing_array[cur_tab].pricingData.child.commission)){
										errors.push({"element":"child_commission","message":"Enter Commission"})
										valid = false
									}
								}
							}
						}
						else{
							// unit based validation
							if(!is_empty(pricing_array[cur_tab].pricingData)){
								if (is_empty(pricing_array[cur_tab].pricingData.unit)) {
									valid = false;
									errors.push({"element":"unit_market_price","message":"Enter Market Price"})
									errors.push({"element":"unit_commission","message":"Enter Commission"})								
								}
								else{
									// validate unit market pricing, for kids ignore validation
									check_valid = validate_market_pricing(pricing_array[cur_tab].pricingData.unit.marketPrice,"unit_market_price",false)
									if(!check_valid.result){
										errors.push(check_valid.errors)
										valid=false
									}

									// validate unit commission , for kids ignore validation
									check_valid = validate_commission(pricing_array[cur_tab].pricingData.unit.commission,"unit_commission",false)
									if(!check_valid.result){
										errors.push(check_valid.errors)
										valid=false
									}
								}
							}
							maximum_required = pricing_array[cur_tab].maximumPersonRequired
							// validation maximum persion required (inventory)
							if(is_empty(maximum_required)){
								valid=false;
								errors.push({"element":"maximumPersonRequired","message":"Please enter maxmium persons allowed per unit"})
							}
							else if(isNaN(maximum_required)){
								valid=false;
								errors.push({"element":"maximumPersonRequired","message":"Please enter valid maxmium persons allowed per unit"})
							} else if(parseInt(maximum_required) > 9999 || parseInt(maximum_required)< 1){
								valid=false;
								errors.push({"element":"maximumPersonRequired","message":"Should be 1 to 999"})
							}
						}
						if(valid==false || errors.length>0){
							scope.$parent.currentTab[saleType] = cur_tab;
							localStorageErrorTab = (cur_tab+1) + ","
							break;
						}
					}
					if(errors.length>0){
						angular.forEach(errors,function(error){
							if(error.element.indexOf("removeBlackOut") > -1) { //error.element=='blackoutdays_validity'){
								jQuery('[name="'+error.element+'"]').append("<span class='pricing_messages'><label class='validation-invalid'>"+error.message+"</label></span>")
							}
							else {
								jQuery('[name="'+error.element+'"]').after("<span class='pricing_messages'><p class='validation-invalid'>"+error.message+"</p></span>")
							}
							localStorageErrorMsg = localStorageErrorMsg + error.element +","
						})
					}
					/*(if(blackOutDaysValid==false && blackouterrors.length>0) {
						localStorageErrorMsg = localStorageErrorMsg + blackouterrors.element +","
					} */

					localStorage.setItem("pricingErrorTab",localStorageErrorTab)
					localStorage.setItem("pricingError",localStorageErrorMsg)
					return (valid && errors.length==0)
				}
			};

			var defaultMsg = {
				required: {
					error: 'Please fill out this field.',
					success: ''
				},
				validate_dynamic_radio:{
					error:'Select the appropriate option.',
					success: ''
				},
				percentage:{
					error:'Percentage should be in 0 - 100',
					success: ''
				},
				ifsc: {
					error:'Please enter a valid IFSC code',
					success: ''
				},
				pan: {
					error:'Please enter a valid PAN Number',
					success: ''
				},
				string: {
					error: 'Please enter valid details.',
					success: ''
				},
				checkBoxRequired: {
					error: 'Please fill out this field.',
					success: ''
				},
				selectRequired: {
					error: 'Please fill out this field.',
					success: ''
				},
				minValue: {
					error: 'Higher value expected.',
					success: ''
				},
				maxValue: {
					error: 'Lower value expected.',
					success: ''
				},
				minDate: {
					error: 'Date should be above minimum.',
					success: ''
				},
				url: {
					error: 'Enter a valid website name.',
					success: ''
				},
				email: {
					error: 'Please enter a valid Email address.',
					success: ''
				},
				number: {
					error: 'Please enter a Vaild Number.',
					success: ''
				},
				phone: {
					error: 'Please enter a Vaild Mobile Number.',
					success: ''
				},
				mobile_IN: {
					error: 'Please enter a Vaild Mobile Number.',
					success: ''
				},
				mobile_AE: {
					error: 'Please enter a Vaild Mobile Number.',
					success: ''
				},
				fileRequired: {
					error: 'Please Upload the File.',
					success: ''
				},
				activityDurationValidation: {
					error: 'Invalid Durations.',
					success: ''
				},
				checkBox: {
					error:'Please select the required options.',
					success:''
				},
				multiSelect: {
					error:'Please select required options.',
					success:''
				},
				validate_dynamic: {
					error:'Please add required options.',
					success:''
				},
				radio: {
					error: 'Select the appropriate option.',
					success: ''
				},
				equals: {
					error: "These passwords don't match.",
					success: ""
				},
				maxlength:{
					error: "Please check the length",
					success: ""
				},
				pricing_dynamic:{
					error: "",
					success: ""
				},
				inventory_pricing_dynamic:{
					error:"",
					success:""
				},
				ignore:{
					error:"",
					success:''
				},
				selectAnyOneRole:{
					error:"Please select only one",
					success:''
				},
				rangeRequired:{
					error:"Invalid value",
					success:""
				}

			};

			// var errorHTML = function(msg){
			//   if(msg=='checkbox')
			//   {
			//     return "<p class='validation-error'>"+msg+"</p>"
			//   }
			//   else{
			//     return "<span class='validation-error'>"+msg+"</span>"
			//   }
			// }

			$validationProvider.setExpression(expression).setDefaultMsg(defaultMsg)//.setErrorHTML(errorHTML);

		//   angular.extend($validationProvider, {
		//     validCallback: function (element){

		//         $(element).parents('.form-group:first').removeClass('has-error');
		//     },
		//     invalidCallback: function (element) {
		//       console.log(element)
		//       $(element).parents('.form-group:first').addClass('has-error');
		//     }
		// });

}
]);
}).call(this);
